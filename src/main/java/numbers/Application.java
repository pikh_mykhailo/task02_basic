package numbers;

import java.util.Scanner;

/**
 * The main class of the program which calculates the sum
 * of even and odd numbersin specific sequences given by the user.
 * Builds a Fibonacci sequence, calculates the percentage of even and
 * odd numbers and outputs data to the console
 *
 * @author Mikhailo Pikh
 * @see NumbersService
 * @see Fibonacci
 */
public class Application {

    /**
     * Method main
     * The Scanner is used for user input data.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Fibonacci fibonacci = new Fibonacci(scanner);
        fibonacci.printFibonacciNumber();
        fibonacci.printPercentOfNumbers();
        scanner.close();
    }


}
