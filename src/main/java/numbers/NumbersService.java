package numbers;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Class of the program which build sequences and calculates the sum of even and odd numbers
 *
 * @author Mikhailo Pikh
 * @see Application
 * @see Fibonacci
 */
public final class NumbersService {

    /**
     * The lower limit of sequence.
     */
   private int lowerLimit = 0;

    /**
     * The upper limit of sequence.
     */
    private int upperLimit = 0;

    /**
     * The sum of odd numbers  of sequence.
     */
    private int sumOfOddNumbers = 0;

    /**
     * The sum of even numbers  of sequence.
     */
    private int sumOfEvenNumbers = 0;

    /**
     * Constructs a LinkedList that will be filled with odd numbers.
     */
    private LinkedList<Integer> listOfOddNumber = new LinkedList<Integer>();

    /**
     * Constructs a LinkedList that will be filled with even numbers.
     */
    private LinkedList<Integer> listOfEvenNumber = new LinkedList<Integer>();

    /**
     * The Scanner what use for user input data.
     */
    private final Scanner scanner;

    /**
     * Constructs a Service what build and calculate number sequence.
     */
    public NumbersService(Scanner scanner) {
        this.scanner = scanner;
        action(scanner);
    }

    /**
     * User enters data
     */
    void getIntervalLimit(Scanner scanner) {
        System.out.println("Please enter lower limit");
        lowerLimit = scanner.nextInt();
        System.out.println("Please enter upper limit");
        upperLimit = scanner.nextInt();
    }

    /**
     * Fill lists by numbers and reverse list of even numbers.
     */
    void fillLists() {
        int temp = 0;
        int size = 0;
        for (int i = lowerLimit; i <= upperLimit; i++) {
            if (i % 2 == 0) {
                listOfEvenNumber.add(i);
            } else {
                listOfOddNumber.add(i);
            }
        }
        size = listOfEvenNumber.size();
        for (int i = 0; i < size; i++) {
            temp = listOfEvenNumber.getFirst();
            listOfEvenNumber.removeFirst();
            listOfEvenNumber.add(size - (i + 1), temp);
        }
    }

    /**
     * Calculate the sum of lists number.
     */
    public int getSumOfNumbers(List<Integer> list) {
        int sumOfNumber = 0;
        for (int i : list) {
            sumOfNumber = sumOfNumber + i;
        }

        return sumOfNumber;
    }

    /**
     * Runs methods for input sequence limit and fill sequence.
     */
    public void action(Scanner scanner) {
        getIntervalLimit(scanner);
        fillLists();
        sumOfEvenNumbers = getSumOfNumbers(listOfEvenNumber);
        sumOfOddNumbers = getSumOfNumbers(listOfOddNumber);
    }

    /**
     * Print data to console.
     */
    void printsNumber() {
        System.out.println("interval of odd numbers " + listOfOddNumber);
        System.out.println("interval of even numbers " + listOfEvenNumber);
        System.out.println("sum of odd numbers " + sumOfOddNumbers);
        System.out.println("sum of even numbers " + sumOfEvenNumbers);
    }

    public LinkedList<Integer> getListOfEvenNumber() {
        return listOfEvenNumber;
    }

    public void setListOfEvenNumber(LinkedList<Integer> listOfEvenNumber) {
        this.listOfEvenNumber = listOfEvenNumber;
    }

    public LinkedList<Integer> getListOfOddNumber() {
        return listOfOddNumber;
    }

    public void setListOfOddNumber(LinkedList<Integer> listOfOddNumber) {
        this.listOfOddNumber = listOfOddNumber;
    }

    public int getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(int lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public int getSumOfEvenNumbers() {
        return sumOfEvenNumbers;
    }

    public void setSumOfEvenNumbers(int sumOfEvenNumbers) {
        this.sumOfEvenNumbers = sumOfEvenNumbers;
    }

    public int getSumOfOddNumbers() {
        return sumOfOddNumbers;
    }

    public void setSumOfOddNumbers(int sumOfOddNumbers) {
        this.sumOfOddNumbers = sumOfOddNumbers;
    }

    public int getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(int upperLimit) {
        this.upperLimit = upperLimit;
    }
}
