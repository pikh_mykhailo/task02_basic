
package numbers;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * Class of the program which build sequences and calculates
 * the sum of even and odd numbers.
 *
 * @author Mikhailo Pikh
 * @see Application
 * @see NumbersService
 */
public final class Fibonacci {

    /**
     * Scanner to input data.
     */
    private final Scanner scanner;
    /**
     * The biggest odd number of fibonacci list.
     */
    private int biggestOddNumber = 0;
    /**
     * The biggest even number of fibonacci list.
     */
    private int biggestEvenNumber = 0;
    /**
     * The size of fibonacci list, entered by user .
     */
    private int sizeOfSet = 0;
    /**
     * The percent of odd numbers of fibonacci list.
     */
    private int percentOfOddNumbers = 0;
    /**
     * The percent of even numbers of fibonacci list.
     */
    private int percentOfEvenNumbers = 0;
    /**
     * The check if fibonacci list size more 2 numbers.
     */
    private boolean checkSizeOfSet = false;
    /**
     * The list in which the fibonacci sequence will be placed.
     */
    private LinkedList<Integer> fibonacciList;

    public Fibonacci(Scanner scanner) {
        this.scanner = scanner;
        inputSizeOfSet();
        if (checkSizeOfSet) {
            calculateFibonacci();
            calculatePercentOfNumbers();
        }
    }

    public LinkedList<Integer> getFibonacciList() {
        return fibonacciList;
    }

    public void setFibonacciList(LinkedList<Integer> fibonacciList) {
        this.fibonacciList = fibonacciList;
    }

    public int getBiggestEvenNumber() {
        return biggestEvenNumber;
    }

    public void setBiggestEvenNumber(int biggestEvenNumber) {
        this.biggestEvenNumber = biggestEvenNumber;
    }

    public int getBiggestOddNumber() {
        return biggestOddNumber;
    }

    public void setBiggestOddNumber(int biggestOddNumber) {
        this.biggestOddNumber = biggestOddNumber;
    }

    public boolean isCheckSizeOfSet() {
        return checkSizeOfSet;
    }

    public void setCheckSizeOfSet(boolean checkSizeOfSet) {
        this.checkSizeOfSet = checkSizeOfSet;
    }

    public int getPercentOfEvenNumbers() {
        return percentOfEvenNumbers;
    }

    public void setPercentOfEvenNumbers(int percentOfEvenNumbers) {
        this.percentOfEvenNumbers = percentOfEvenNumbers;
    }

    public int getPercentOfOddNumbers() {
        return percentOfOddNumbers;
    }

    public void setPercentOfOddNumbers(int percentOfOddNumbers) {
        this.percentOfOddNumbers = percentOfOddNumbers;
    }

    public int getSizeOfSet() {
        return sizeOfSet;
    }

    public void setSizeOfSet(int sizeOfSet) {
        this.sizeOfSet = sizeOfSet;
    }

    /**
     * The user enters the size of fibonacci sequence.
     */
    public void inputSizeOfSet() {
        System.out.println("Please enter size of Fibonacci set");
        sizeOfSet = scanner.nextInt();
        if (sizeOfSet < 2) {
            System.err.println("Please enter correct size");
            inputSizeOfSet();
        } else if (sizeOfSet == 2) {
            checkSizeOfSet = true;
            biggestOddNumber = 1;
            percentOfEvenNumbers = 0;
            percentOfOddNumbers = 100;
        } else {
            checkSizeOfSet = true;
        }
        scanner.close();
    }

    /**
     * The method that build fibonacci sequence
     * and lists it into  fibonacciList.
     */
    public void calculateFibonacci() {
        int n0 = 1;
        int n1 = 1;
        int n2;
        fibonacciList = new LinkedList<Integer>();
        fibonacciList.add(1);
        fibonacciList.add(1);
        for (int i = 3; i <= sizeOfSet; i++) {
            n2 = n0 + n1;
            fibonacciList.add(n2);
            n0 = n1;
            n1 = n2;
        }
        System.out.println(fibonacciList);
    }

    /**
     * Set  biggestEvenNumber and  biggestOddNumber
     * from numbers of fibonacci sequence.
     */
    public void setLastNumbers() {
        int lastNumber = 0;
        int secondLastNumber = 0;
        lastNumber = fibonacciList.getLast();
        secondLastNumber = fibonacciList.get(sizeOfSet - 2);
        if (lastNumber % 2 == 0) {
            biggestEvenNumber = lastNumber;
            biggestOddNumber = secondLastNumber;
        } else if (sizeOfSet != 2) {
            biggestOddNumber = lastNumber;
            biggestEvenNumber = secondLastNumber;
            if (biggestEvenNumber % 2 != 0) {
                biggestEvenNumber = fibonacciList.get(sizeOfSet - 3);
            }
        }
    }

    /**
     * Print fibonacci sequence into console.
     */
    public void printFibonacciNumber() {
        setLastNumbers();
        if (sizeOfSet == 2) {
            System.out.println("Biggest odd number: " + biggestOddNumber);
            System.out.println("Biggest even number: none");
        } else {
            System.out.println("Biggest odd number: " + biggestOddNumber);
            System.out.println("Biggest even number: " + biggestEvenNumber);
        }
    }

    /**
     * Calculate percent of odd and even numbers,
     * what contains into fibonacci sequence.
     */
    public void calculatePercentOfNumbers() {
        float sumOfOddNumbers = 0;
        float sumOfEvenNumbers = 0;
        final int numberToPercent = 100;

        for (int i : fibonacciList) {
            if (i == 1 || i % 2 != 0) {
                sumOfOddNumbers++;
            } else {
                sumOfEvenNumbers++;
            }
        }
        percentOfEvenNumbers = Math.round((sumOfEvenNumbers / sizeOfSet)
                * numberToPercent);
        percentOfOddNumbers = Math.round((sumOfOddNumbers / sizeOfSet)
                * numberToPercent);
    }

    /**
     *  Print  percentOfOddNumbers and  percentOfEvenNumbers
     * into console.
     */
    public void printPercentOfNumbers() {
        System.out.println("percent of odd number: "
                + percentOfOddNumbers + "%");
        System.out.println("percent of even number: "
                + percentOfEvenNumbers + "%");
    }
}
